// ============== \\
//  Unit testing  \\
// ============== \\

\d .test
/ Meta
M.FAMILY :`stl;
M.NAME   :`test;
M.VERSION:`0.001.0;
M.MODULES:enlist(`stl.worker`0.001.0);

/ Special
SELF:system"d";
BB  :.miq.bb.new[SELF];

/ Object definition
O.NAME    :.miq.object.define[1b;`test.name;1#`name;1#`s];
O.STEP    :.miq.object.extend[1b;`test.name;`test.step    ;1#`step    ;1#`j ];
O.SCENARIO:.miq.object.extend[1b;`test.step;`test.scenario;1#`scenario;1#`s ];
O.CASE    :.miq.object.extend[1b;`test.step;`test.case    ;1#`case    ;1#`j ];
O.PLAN    :.miq.object.extend[1b;`test.case;`test.plan    ;1#`scenario;1#`sl];
.miq.object.import[`test.;` sv SELF,`o];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
/ Standard
INIT:(` sv BB,`init) set 0b;

/ Static
SCENARIOS :(` sv BB,`scenarios ) set `build`smash;
SECTIONS  :(` sv BB,`sections  ) set `name`include`coverage`augment`step`case,SCENARIOS[];
PROPERTIES:(` sv BB,`properties) set `pass`fail`success`is`not`count;
DIR.IGNORE:(` sv BB,`dir`ignore) set `include`augment`build`smash;

/ Current state during test parsing
NOW.ROOT:` sv BB,`now`root;
NOW.FILE:` sv BB,`now`file;
NOW.DIR :` sv BB,`now`dir;
NOW.NAME:` sv BB,`now`name;
NOW.STEP:` sv BB,`now`step;
NOW.CASE:` sv BB,`now`case;

/ State tables
/AUGMENT  :` sv BB,`augment;
COVERAGES:` sv BB,`coverages;
STEPS    :` sv BB,`steps;
BUILD    :` sv BB,`build;
SMASH    :` sv BB,`smash;
CASES    :` sv BB,`cases;
PLANS    :` sv BB,`plans;
EVALS    :` sv BB,`evals;
RESULTS  :` sv BB,`results;
ERRORS   :` sv BB,`errors;
QUEUE    :` sv BB,`queue;
BATCHES  :` sv BB,`batches;

/ Table template
/TT.AUGMENT  :2!flip`name`step`description`lang`code`gist!"SJ****"$\:();
TT.COVERAGES:o.name.tt[]!flip`description`source`code!"*S*"$\:();
TT.COVERAGE :o.name.tt[]^flip`item`tested`case!"SB*"$\:();
TT.STEPS    :o.step.tt[]!flip`description`source!"*S"$\:();
TT.SCENARIOS:o.scenario.tt[]!flip`description`source`code!"*S*"$\:();
TT.CASES    :o.case.tt[]!flip`nature`description`function`input`method`assert`loop`build`smash`source!"S*****JBBS"$\:();
TT.PLANS    :o.plan.tt[]!flip`build`smash`input`function`loop!"****J"$\:();
TT.EVALS    :o.plan.tt[]!flip`stage`function`input`method`assert`output!"******"$\:();
TT.ERRORS   :o.plan.tt[]!flip`error`stack!"**"$\:();
TT.RESULTS  :o.plan.tt[]!flip`pass`time`space`executed`failed`evaluation`build`execution`smash`assertion!"BNJBBBBBBB"$\:();
TT.QUEUE    :o.plan.tt[]!flip`batch`executed`evaluated`completed`terminated`start`finish!"SBBBBPP"$\:();
TT.BATCHES  :1!flip`name`worker`cases`initiated`ongoing`completed`terminated`start`finish!"SJJBBBBPP"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.test.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.test.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.test.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`name.composite      ;1b                   )
 ;(`name.short          ;1b                   )
 ;(`name.extension      ;0b                   )
 ;(`file.absolute       ;1b                   )
 ;(`file.extension      ;1#`miqt              )
 ;(`auto.build          ;1b                   )
 ;(`auto.smash          ;1b                   )
 ;(`eval.serialise      ;0b                   )
 ;(`execute.local       ;0b                   )
 ;(`execute.debug       ;0b                   )   / TODO implies local and disables trapping to allow interractive debugging / or maybe simply don't kill worker and boot up console targeting the worker
 ;(`execute.serial      ;0b                   )
 ;(`remote.timeout      ;60000                )   / NOTE how to make timeout possible on local without C code... :(
 ;(`remote.workers      ;3                    )
 ;(`remote.timer        ;50                   )
 ;(`remote.keepalive    ;0b                   )   / TODO debug option, keep a handle open to worker to prevent them from being killed so we can inspect it
/ ;(`dump.enable         ;0b                   )   / TODO dump process state on test failure
/ ;(`dump.path           ;`:.                  )
 ;(`output.persist      ;0b                   )
 ;(`output.summary      ;0b                   )
 ;(`output.file         ;`:miq.test.output    )
 ;(`complete.exit       ;1b                   )
 ;(`complete.exitcode   ;31                   )
 );

.test.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.test.config.assemble]
    each `name.composite`name.short`name.extension
   ,`file.absolute`file.extension`auto.build`auto.smash`eval.serialise
   ,`execute.local`execute.debug`remote.timeout`remote.workers`remote.timer`execute.serial
   /,`dump.enable`dump.path
   ,`output.persist`output.summary`output.file`complete.exit`complete.exitcode;
 };

// ==========
//  Messages
// ==========
.test.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,](
/    (`augment   ;"No such augment definition: "   )
    (`name      ;"No such test name: "            )
   ;(`step      ;"No such step definiton: "       )
   ;(`scenario  ;"No such test scenario: "        )
   ;(`case      ;"No such test definiton: "       )
   ;(`plan      ;"No such test plan: "            )
   ;(`property  ;"No such property definition: "  )
   ;(`batch     ;"No such batch of tests: "       )
  );
 };

// ===========
//  Interface
// ===========
.test.i.interface:{[]
  api:.miq.ns.prune[`h`o`i`t`z`C`L`config] .miq.ns.trim[``init] .miq.ns.tree[`.test];
  api:exec name from group api;
  api:api where not .miq.syntax.name.is.constant'[api];

  handler    :` sv'`stl,'`$1_'string api;
  implementer:api;
  arity      :.miq.fn.arity'[api];

  / TODO Make this nice and describe every function!
  .miq.handler.define[1b;;;;"!!! Description missing !!! TEST module public interface dump"]'[handler;implementer;arity]
 };

// =========
//  General
// =========
.test.init:{[fnConfigLoader]
  .miq.handler.import[;` sv SELF,`h]'[`stl.log`stl.trap`stl.ipc`stl.worker];

  .test.h.log.info "Initialising: ",-3!SELF;

  .test.i.messages[];
  .test.i.interface[];

  COVERAGES set @[get;COVERAGES;TT.COVERAGES];
  STEPS     set @[get;STEPS    ;TT.STEPS    ];
  BUILD     set @[get;BUILD    ;TT.SCENARIOS];
  SMASH     set @[get;SMASH    ;TT.SCENARIOS];
  CASES     set @[get;CASES    ;1#TT.CASES  ];
  PLANS     set @[get;PLANS    ;TT.PLANS    ];
  EVALS     set @[get;EVALS    ;1#TT.EVALS  ];
  ERRORS    set @[get;ERRORS   ;TT.ERRORS   ];
  RESULTS   set @[get;RESULTS  ;TT.RESULTS  ];
  QUEUE     set @[get;QUEUE    ;TT.QUEUE    ];
  BATCHES   set @[get;BATCHES  ;TT.BATCHES  ];

  NOW.ROOT  set @[get;NOW.ROOT ;`           ];
  NOW.FILE  set @[get;NOW.FILE ;`           ];
  NOW.DIR   set @[get;NOW.DIR  ;`           ];
  NOW.NAME  set @[get;NOW.NAME ;`           ];
  NOW.STEP  set @[get;NOW.STEP ;0           ];
  NOW.CASE  set @[get;NOW.CASE ;0           ];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .test.config.apply[];

  INIT set 1b;
  .test.h.log.info "Initialisation completed: ",-3!SELF;

  / Has to run last
  / TODO run twice on first run to capture itself
  / TODO create filters so it won't attempt to delete names in certain contexts
  /AUGMENT set @[get;AUGMENT;{AUGMENT set TT.AUGMENT; update gist:enlist .miq.ns.gist[] from 1#TT.AUGMENT}];
 };

/ Clear out the state
.test.clear:{[sName]
  $[.is.none[sName];
    [
      .test.h.log.info "Clearing all test definitions";
      /AUGMENT   set 1#AUGMENT;
      COVERAGES set TT.COVERAGES;
      STEPS     set TT.STEPS;
      BUILD     set TT.SCENARIOS;
      SMASH     set TT.SCENARIOS;
      CASES     set 1#TT.CASES;
      EVALS     set 1#TT.EVALS;
      ERRORS    set TT.ERRORS;
      RESULTS   set TT.RESULTS;
    ];
    [
      .test.is.error.name[sName];
      .test.h.log.info "Clearing test definitions: ",-3!sName;
      /delete from AUGMENT   where name=sName;
      delete from COVERAGES where name=sName;
      delete from STEPS     where name=sName;
      delete from BUILD     where name=sName;
      delete from SMASH     where name=sName;
      delete from CASES     where name=sName;
      delete from EVALS     where name=sName;
      delete from ERRORS    where name=sName;
      delete from RESULTS   where name=sName;
    ]
  ];
 };

/ Run after all test plans have been executed
.test.complete:{[]
  .test.h.log.info "Execution completed!\n",.Q.s .test.summary[];
  .test.h.log.info "Code coverage: ",-3!exec 100*count[where tested]%count[tested] from .test.coverage[];

  if[C.OUTPUT.PERSIST[];
    .test.h.log.info "Generating output file: ",-3!C.OUTPUT.FILE[];
    C.OUTPUT.FILE[] set .test.output[] $[C.OUTPUT.SUMMARY[]; `summary; ::];
  ];

  if[C.COMPLETE.EXIT[] & not[.miq.system.is.terminal[]] & 0~count .z.W;
    exitcode:$[count select from .test.summary[] where not null failure, 0<>failure; C.COMPLETE.EXITCODE[]; 0];
    .test.h.log.info "Automatic exit using exit code: ",-3!exitcode;
    exit exitcode
  ];
 };

/ Persist output
.test.output:{[]
  (!). flip(
    (`coverages ;COVERAGES[]      )
   ;(`steps     ;STEPS[]          )
   ;(`build     ;BUILD[]          )
   ;(`smash     ;SMASH[]          )
   ;(`cases     ;CASES[]          )
   ;(`plans     ;PLANS[]          )
   ;(`evals     ;EVALS[]          )
   ;(`errors    ;ERRORS[]         )
   ;(`results   ;RESULTS[]        )
   ;(`queue     ;QUEUE[]          )
   ;(`batches   ;BATCHES[]        )
   ;(`report    ;.test.report[]   )
   ;(`coverage  ;.test.coverage[] )
   ;(`summary   ;.test.summary[]  )
  )
 };

/ Testing report
.test.report:{[]
  select all pass, scenarios:count scenario, success:count where pass, failure:count where not pass by name,step,case from .test.results[]
 };

/ Testing summary
.test.summary:{[]
  summary:select cases:count pass, passed:sum pass, sum scenarios, sum success, sum failure by name from .test.report[];
  if[.is.empty[summary];
    summary:summary upsert
      (select cases:count case by name from .test.cases[]) lj
      (select scenarios:count scenario by name from .test.plan key .test.cases[]);
  ];

  summary uj select coverage:100*count[where tested]%count[tested] by name from .test.coverage[]
 };

/ Run coverage
.test.coverage:{[oName]
  $[.is.none[oName]     ; $[.is.empty[key COVERAGES]; TT.COVERAGE; raze .test.i.coverage'[key COVERAGES]];
    .test.is.name[oName]; .test.i.coverage[oName];
                          '.test.h.log.error "Cannot return coverage for: ",-3!oName
  ]
 };

.test.i.coverage:{[oName]
  if[not oName in key COVERAGES; :TT.COVERAGE];

  domain:.miq.qk.interpret["q"] each COVERAGES[oName][`code];
  domain:last domain where not (::)~/:domain;

  if[not .is.nsl[domain]; '.test.h.log.error "Coverage statement did not return a list of addresses: ",-3!oName];

  names :o.name.construct'[key CASES];
  cases :select from CASES where oName~/:names;
  funcs :exec raze each function from cases;
  plain :any each'funcs ~\:'/: domain;

  lmatch:"*[^",(.miq.u.text.Aan,"."),"]";
  rmatch:"[^",(.miq.u.text.Aan,"._"),"]*";
  hidden:any each'string[funcs] like\:/: lmatch,/:string[domain],\:rmatch;

  TT.COVERAGE upsert oName[`name],/:{(y;0<>count where z;x where z)}[key cases]'[domain;plain|hidden]
 };

/ Accessors
/.test.augments :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; AUGMENT[]  ; select from AUGMENT   where name=sName]};
.test.coverages:{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; COVERAGES[]; select from COVERAGES where name=sName]};
.test.steps    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; STEPS[]    ; select from STEPS     where name=sName]};
.test.build    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; BUILD[]    ; select from BUILD     where name=sName]};
.test.smash    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; SMASH[]    ; select from SMASH     where name=sName]};
.test.cases    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; 1_CASES[]  ; select from 1_CASES   where name=sName]};
.test.plans    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; PLANS[]    ; select from PLANS     where name=sName]};
.test.evals    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; 1_EVALS[]  ; select from 1_EVALS   where name=sName]};
.test.errors   :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; ERRORS[]   ; select from ERRORS    where name=sName]};
.test.results  :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; RESULTS[]  ; select from RESULTS   where name=sName]};
.test.queue    :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; QUEUE[]    ; select from QUEUE     where name=sName]};
.test.batches  :{[sName] .is.error.any[`none`s;sName]; $[.is.none[sName]; BATCHES[]  ; select from BATCHES   where name=sName]};

/ Checks
/.test.is.augment :{[x] @[{x in key AUGMENT};x;0b]};
.test.is.name    :{[oName] $[.miq.object.is.class[O.NAME;oName]; oName[`name] in exec name from STEPS; 0b]};
.test.is.step    :{[oStep] $[.miq.object.is.class[O.STEP;oStep]; oStep in key STEPS; 0b]};
/ TODO cannot use scenario in .test.get.build/smash due to handling non-existent scenarios...
.test.is.scenario:{[oScenario] $[.miq.object.is.class[O.SCENARIO;oScenario]; oScenario in key BUILD; 0b]};
.test.is.case    :{[oCase] $[.miq.object.is.class[O.CASE;oCase]; oCase in key CASES; 0b]};
.test.is.plan    :{[oPlan] $[.miq.object.is.class[O.PLAN;oPlan]; oPlan in key PLANS; 0b]};
.test.is.property:{[x] $[.is.s[x]; x in PROPERTIES[]; 0b]};
.test.is.batch   :{[x] $[.is.s[x]; x in key BATCHES; 0b]};

// ===================
//  Loading & Parsing
// ===================
// Utils
.test.i.now.file:{[fs] $[C.FILE.ABSOLUTE[]; .miq.fs.to.absolute[fs]; fs]}

.test.i.now.name:{[fs]
  path:`dir`file!` vs fs;

  if[not C.NAME.EXTENSION[]; path[`file]:.miq.fs.file.split[path`file][0]];
  if[C.NAME.SHORT[];
    if[null NOW.ROOT[]; :path[`file]];
    path[`dir]:.miq.fs.path.relative[NOW.ROOT[];path`dir]
  ];

  $[C.NAME.COMPOSITE[]; ` sv {x except ``.`..}`$"/"vs 1_string` sv get path; path`file]
 };

.test.i.level.top :{[x] if[0<>NOW.STEP[]; '.test.h.log.error "Section cannot be nested under STEP: ",-3!x]};
.test.i.level.step:{[x] if[0 ~NOW.STEP[]; '.test.h.log.error "Section has to be nested under STEP: ",-3!x]};

// Loading
.test.explore:{[fs]
  .miq.fs.is.error.dir[fs];

  tree:.miq.fs.tree[fs];
  dir :exec dir from group tree;
  dir :dir where (` vs'dir)[;1] in DIR.IGNORE[];

  file:exec file from group tree;
  file:file where not any each file like/:\:string[dir],\:"*";
  file where (.miq.fs.file.split@'(` vs'file)[;1])[;1] in C.FILE.EXTENSION[]
 };

.test.load:{[fs]
  .is.error.fs[fs];
  .miq.fs.is.error.present[fs];

  / Find test files when directory
  if[.miq.fs.is.dir[fs];
    if[root:null NOW.ROOT[]; NOW.ROOT set .miq.fs.to.absolute[fs]];

    .test.h.log.info "Exploring directory for test case files: ",-3!fs;
    files:.test.explore[fs];
    .test.h.log.info "Discovered [",(-3!count files),"] files: ",-3!files;

    files:.z.s'[files];
    if[root; NOW.ROOT set `];
    :files
  ];

  / Load a single test file
  NOW.FILE set .test.i.now.file[fs];
  NOW.DIR  set first` vs NOW.FILE[];
  NOW.NAME set .test.i.now.name[fs];

  .test.h.log.info "Loading test definitions: ",-3!`test`file!get each NOW[`NAME`FILE];
  if[.test.is.name[NOW.NAME[]]; .test.clear[NOW.NAME[]]];
  .test.i.load[NOW.FILE[];1b];
  set'[NOW`FILE`DIR`NAME`STEP`CASE;```,0 0];
  fs
 };

.test.i.load:{[fs;bInclude]
  cll:read0 fs;
  sections:(^/)?'[cll like\:/:(upper string SECTIONS[]),\:":*";SECTIONS[];`];
  sections:(sections except `)!(where not null sections) cut cll;

  / TODO implement better INCLUDE nesting
  if[not bInclude;
    if[`include in key sections; .test.h.log.warn "Ignoring nested INCLUDE section"];
    sections:`include _ sections;
  ];

  {[section;code]
    $[section in `name`augment`coverage; .test.i.level.top[section];
      section in `case                 ; .test.i.level.step[section];
    ];
    .test.parse[section][code]
  }'[key sections;get sections]
 };

// Name
.test.parse.name:{[cll]
  .is.error.cl'[cll];

  name:`$.miq.u.trim 5_raze .miq.u.text.uncomment[" /"]'[cll];
  .test.h.log.info "Loading test name: ",-3!name;
  NOW.NAME set name;
  if[.test.is.name[name]; .test.clear[name]];
 };

// Include
.test.parse.i.file:{[sSection;cll]
  file:.miq.u.text.uncomment[" /"]'[cll];
  file:@[file;0;:;(1+file[0]?":")_file 0];
  file:.miq.u.trim'[file];

  file:raze {[l] $[.is.empty[l]; 0#`; "`:"~2#l; get l; -1!`$l]} each file;
  file:` sv'NOW.DIR[],'sSection,'last each` vs'file;

  if[.is.empty[file]; :`$()];
  missing:file where not .miq.fs.is.file'[file];
  if[not .is.empty[missing]; '.test.h.log.error "No such file to include: ",-3!missing];

  (')[.miq.fs.to.relative;.miq.fs.path.real] each file
 };

.test.parse.include:{[cll]
  .is.error.cl'[cll];

  file:.test.parse.i.file[`include;cll];
  if[.is.empty[file]; .test.h.log.warn "No include file specified!"; :(::)];

  .test.parse.i.include'[file];
 };

.test.parse.i.include:{[fs]
  previous:NOW.FILE[];
  NOW.FILE set .test.i.now.file[fs];

  .test.h.log.info "Include file: ",-3!NOW.FILE[];
  .test.i.load[NOW.FILE[];0b];

  NOW.FILE set previous;
 };

// Coverage
.test.parse.coverage:{[cll]
  .is.error.cl'[cll];

  .test.h.log.info "Loading coverage definition: ",-3!NOW.NAME[];
  body   :1_cll;
  comment:(("\""~/:first each body)?0b)#body;
  code   :.miq.qk.process[-1!NOW.NAME[];0N;] .miq.u.text.uncomment[" /"]'[count[comment] _ body];

  comment:$[.is.empty[comment]; ""; .miq.u.trim -1_` sv 1_'comment];
  COVERAGES upsert (NOW.NAME[];comment;NOW.FILE[];code)
 };

// Augment
/ TODO allow passing in files, read from augment/ dir
.test.parse.augment:{[cll]
  .is.error.cl'[cll];

  .test.h.log.info "Augmenting miQ.test codebase: ",-3!get'[NOW`NAME`STEP`CASE];

  file:.test.parse.i.file[`augment;1#cll];
  if[.is.empty[file]; :.miq.qk.interpret["q"] each .miq.qk.process[`;0N;1_cll]];

  raze {[x]
    .test.h.log.info "Sourcing augmentation code from: ",-3!x;
    .miq.qk.interpret["q"]'[.miq.qk.load x]
  }'[file]
 };

// Step
.test.parse.step:{[cll]
  .is.error.cl'[cll];

  number:.miq.u.trim .miq.u.text.uncomment[" /"] 5_first cll;
  NOW.STEP set $[.is.empty[number];1+NOW.STEP[];"J"$number];
  .test.h.log.info "Loading test step: ",-3!o.step.construct[(NOW.NAME[];NOW.STEP[])];

  comment:(("\""~/:first each 1_cll)?0b)#1_cll;
  comment:$[.is.empty[comment]; ""; .miq.u.trim -1_` sv 1_'comment];
  STEPS upsert (NOW.NAME[];NOW.STEP[];comment;NOW.FILE[]);
 };

// Scenario
.test.parse.i.leader:{[sSection;cl]
  def:.miq.u.text.uncomment[" /";cl];
  def:.miq.u.trim (1+def?":")_def;

  scenario:{[cl] $["`"~cl 0; get cl; `$cl]};
  file:{[cl] $["`:"~2#cl; get cl; -1!`$cl]};

  def:$[
    .is.empty[def]; (`default;`);
    " " in def    ; (scenario;file)@'" " vs def;
    "`:"~2#def    ; (`default;file[def]);
                    (scenario[def];`)
  ];

  if[null file:last def; :def];
  file:` sv NOW.DIR[],sSection,`$1_string file;
  .miq.fs.is.error.file[file];

  (def 0;) .miq.fs.to.relative .miq.fs.to.absolute[file]
 };

.test.parse.i.scenario:{[sScenario;cll]
  if[not sScenario in SCENARIOS[]; '.test.h.log.error "No such scenario: ",-3!sScenario];
  .is.error.cl'[cll];

  def :.test.parse.i.leader[sScenario;first cll];
  name:def 0;   / Scenario name
  file:def 1;   / Scenario code source

  .test.h.log.info string[sScenario]," scenario: ",-3!name;
  body   :1_cll;
  comment:(("\""~/:first each body)?0b)#body;
  code   :$[null file;
      [file:NOW.FILE[]; count[comment]_body];
      [.test.h.log.info "Sourcing ",string[sScenario]," code: ",-3!file; read0 file]
  ];

  descriptor:-1!` sv NOW.NAME[],(`$string NOW.STEP[]),sScenario;
  code:.miq.qk.process[descriptor;0N;code];

  comment:$[.is.empty[comment]; ""; .miq.u.trim -1_` sv 1_'comment];
  SELF[upper sScenario] upsert (NOW.NAME[];NOW.STEP[];name;comment;file;code);
 };

.test.parse.build:.test.parse.i.scenario[`build];
.test.parse.smash:.test.parse.i.scenario[`smash];

// Case
/ IDEA L expands to fit in more in/out tests for same operation?
/      or rather auto-detect multi input / assert pairs
/      or simply add an optional field before function to define flags?
/ TODO implement flag retrieval during .test.parse.i.test[]
/ TODO rename 'tests' to 'cases' and make it singular
.test.parse.case:{[cll]
  .is.error.cl'[cll];

  / Protect from NAME being changed within STEP definition
  .test.is.error.step o.step.construct[(NOW.NAME[];NOW.STEP[])];

  code:.miq.u.text.uncomment[" /"]'[1_cll];
  code:.miq.u.trim'[code];
  code:code where not .is.empty'[code];

  / TODO 'rich' parsing without loops and temp variables???
  /      Make this FUGLY code nicer!!!
  p:{if[not";"~last x:.miq.u.trim[x];x,:";"]; $[(::)~last x:1_-5!x;-1_x;x]};
  cases:(); i:0; t:("";());
  fields:7; fill:0#0; ingest:fields-count fill;

  while[count code;
    l:first code;
    $["\""~first l             ; t:(t[0],(1_l),"\n";t[1]);
      "#"~first l              ; [.is.error.jl fill:$[(::)~get 1_l;0#fill;(),get 1_l];ingest:fields-count fill];
      ingest>=count c:t[1],p[l]; t:(t[0];c);
                                 '.test.h.log.error "Invalid test case: ",-3!(t[0];c)
    ];
    if[ingest=count c; c:(); i+:1; cases,:enlist (-1_t[0];t[1],fill); t:("";())];
    code:1_code;
  ];

  / Exit if test section defines no cases
  if[.is.empty[cases]; :(::)];

  if[C.AUTO.BUILD[]; cases:.[cases;(0;1;5);:;1]];
  if[C.AUTO.SMASH[]; cases:.[cases;(count[cases]-1;1;6);:;1]];

  .test.parse.i.case .'cases;
  NOW.CASE set 0;
 };

/ TODO check that individual values are valid
.test.parse.i.case:{[clDescription;case]
  -6!(+:;NOW.CASE;1); / Increment case iterator
  description:.miq.u.trim[clDescription];

  .test.h.log.info "Loading test case: ",-3!o.case.construct[(NOW.NAME[];NOW.STEP[];NOW.CASE[])];

  / TODO think of a better way to handle 'count' property name clash with q primitive?
  if[count ~ case 2; case[2]:`count];

  definition:(!). flip (
    (`name        ;NOW.NAME[]                                 )
   ;(`step        ;NOW.STEP[]                                 )
   ;(`case        ;NOW.CASE[]                                 )
   ;(`nature      ;`value`property .test.is.property[case 2]  )
   ;(`description ;description                                )
   ;(`function    ;case[0]                                    )   / K tree
   ;(`input       ;case[1]                                    )   / K tree
   ;(`method      ;case[2]                                    )   / K tree
   ;(`assert      ;case[3]                                    )   / K tree
   ;(`loop        ;`long$case[4]                              )
   ;(`build       ;`boolean$case[5]                           )
   ;(`smash       ;`boolean$case[6]                           )
   ;(`source      ;NOW.FILE[]                                 )
  );

  if[not definition[`nature] in `property`value; '.test.h.log.error "Invalid test case nature: ",-3!definition`nature];
  if[0>definition[`loop]; '.test.h.log.error "Loop value has to be a positive number: ",-3!definition`loop];
  if[not .is.b[definition`build]; '.test.h.log.error "Build has to be a boolean atom: ",-3!definition`build];
  if[not .is.b[definition`smash]; '.test.h.log.error "Smash has to be a boolean atom: ",-3!definition`smash];

  CASES upsert definition;

  / TODO is this the right place and right way to do this?
  / pre-fill EVAL with serialised values
  if[C.EVAL.SERIALISE[]; EVALS upsert get[o.case.construct[definition]],-8!'`,5#()];
 };

// ==============
//  Augmentation
// ==============
/ NOTE Not used at the moment!
.test.augment.enact:{[augment]
  :(::);
  .test.is.error.augment[augment];

  .test.h.log.info "Enacting augment code: ",-3!augment;
  /.test.get.augment:{[augment]
  /  s:$[(augment 0;0) in key AUGMENT; AUGMENT[(augment 0;0)][`code]; ()];
  /  s,$[ augment      in key AUGMENT; AUGMENT[ augment     ][`code]; ()]
  / };

  def:AUGMENT[augment][`lang`code];
  .miq.qk.interpret[def 0]'[def 1];
  AUGMENT upsert augment,(1#`gist)!enlist .miq.ns.gist[];
  augment
 };

/ TODO How to make this work reliably? Do we need a proper directory snapshot, instead of a simple gist?
.test.augment.repeal:{[augment]
  :(::);
  .test.is.error.augment[augment];

  .test.h.log.info "Repealing augment code: ",-3!augment;

  diff:.miq.ns.diff . AUGMENT[flip(`,0N),'augment][`gist];
  if[not .is.empty[diff`removed]; .test.h.log.warn "Augment has removed some items! ",-3!`augment`removed!(augment;diff`removed)];
  if[not .is.empty[diff`changed]; .test.h.log.warn "Augment has changed some items! ",-3!`augment`removed!(augment;diff`changed)];

  if[not .is.empty[diff`added];
    .test.h.log.info "Removing items created by augmentation: ",-3!diff`added;
    / TODO Deal with this later
    /{if[0N!.miq.ns.is.present[x]; .miq.ns.delete[x]]} each diff`added
    /{@[.miq.ns.delete;x;{.test.h.log.warn "Unable to delete: ",-3!y}[;x]]} each diff`added    / TODO instead of failure protection, make sure the miQ is configured properly or iterate over items manually
  ];

  augment
 };

// ======================
//  Planning & Execution
// ======================
/ Data retrieval
.test.get.build:{[oScenario] $[oScenario in key BUILD; BUILD[oScenario][`code]; ()]};
.test.get.smash:{[oScenario] $[oScenario in key SMASH; SMASH[oScenario][`code]; ()]};

.test.get.eval:{[oPlan;s]
  .test.is.error.plan[oPlan];
  .is.error.s[s];

  if[not s in `function`input`method`assert`output; '.test.h.log.error "No such column in EVAL table: ",-3!s];
  $[C.EVAL.SERIALISE[];-9!;::] EVALS[oPlan][s]
 };

/ Run the plan-execute-assert chain
.test.run:{[oCase]
  .is.error.one[(`none;`qd;`qt;`s;`s`j;`s`j`j);oCase];

  cases:key $[
    .is.none[oCase]; 1_CASES[];
    .is.qd[oCase]  ; [.test.is.error.case[oCase]; enlist oCase];
    .is.qt[oCase]  ; [.test.is.error.case'[oCase]; oCase];
                     ?[CASES;.miq.fq.d2cond[(=)](!). count[oCase]#'(o.case.signature[];oCase);0b;()]
  ];

  if[.is.empty[cases]; '.test.h.log.error "No test cases matched: ",-3!cases];

  / Pre-fill results so termination will be seen as test cases not passing
  / TODO clear results for test plans we are about to execute
  plans:.test.plan[cases];
  RESULTS set TT.RESULTS upsert update pass:0b from plans;

  $[C.EXECUTE.LOCAL[];
    [
      .test.execute[plans];
      .test.evaluate[plans];
    ];
    [
      .test.remote.terminate[];
      .test.remote.queue[plans];
      $[C.EXECUTE.SERIAL[]; .test.remote.serial[]; .test.remote.start[C.REMOTE.TIMER[]]]
    ]
  ];
 };

/ TODO .test.prepare[oPlan] to wipe out state tables and seed them with new scenarios?
/.test.prefill:{[oPlan]
/ };

/ Plan test case execution
.test.plan:{[oCase]
  cases:$[.is.qt[oCase]; oCase; enlist oCase];
  .test.is.error.case'[cases];

  scenarios:raze .test.i.expand'[exec distinct name from cases];
  scenarios:scenarios lj select case by name,step from cases;

  plans:raze {update scenario:count[x`case]#enlist x`scenario from flip[`scenario _x]} each scenarios;
  plans:$[.is.none[plans]; key TT.PLANS; plans];
  PLANS set TT.PLANS upsert plans!.test.i.plan'[plans];
  plans
 };

.test.i.expand:{[sName]
  s:exec step from .test.steps[] where name=sName;
  t:select from .test.build[] where name=sName;

  s1:exec scenario from t where step=0;
  s1:$[.is.empty[s1]; (),`; s1];

  s2:s#exec scenario by step from t where step<>0;
  s2:{$[.is.empty[x]; (),`; x]} each s2;

  o.plan.tt[] upsert raze {((2#x),0N),/:enlist'[last x]} each sName,'flip (key;get)@\:{x cross y}[s1] each s2
 };

.test.i.plan:{[oPlan]
  case:CASES[o.case.construct[oPlan]];

  scenarios:o.scenario.construct each flip @[oPlan;`step;:;0,oPlan`step];
  build:$[case`build; raze .test.get.build'[scenarios]; ()];
  smash:$[case`smash; raze .test.get.smash'[scenarios]; ()];

  plan:(!). flip (
    (`build     ;build          )
   ;(`smash     ;smash          )
   ;(`input     ;case`input     )
   ;(`function  ;case`function  )
   ;(`loop      ;case`loop      )
  )
 };

/ Local test case plan execution
/ TODO replace do[] with a loop to repeatedly build & smash to allow performance testing of functions that change state
.test.execute:{[oPlan]
  plans:$[.is.qt[oPlan]; oPlan; enlist oPlan];
  .test.is.error.plan'[plans];

  {[plan;instructions]
    .test.h.log.info "Executing test plan locally: ",-3!plan;
    .test.i.execute[.miq.qk.interpret;get .test.h.trap.apply;plan;instructions]
  }'[plans;PLANS[plans]];

  .test.complete[];
 };

/ remote safe, defined in root context
\d .
.test.i.execute:{[interpret;trap;oPlan;qdInstructions]
  reply:{[oPlan;stage;fail;out]
    neg[.z.w](`.test.worker.reply;oPlan;stage;fail;out);
    if[0i<>.z.w; neg[.z.w][]];
  }[oPlan];

  / 1.) build
  build:trap[interpret["q"]@';qdInstructions`build];
  if[first build; reply[`build;;] . build; :(::)];

  / 2.) K-tree evaluation
  reply[`function;;] . function:trap[-6!;qdInstructions`function];
  if[first function; :(::)];
  reply[`input;;]    . input   :trap[-6!;qdInstructions`input];
  if[first input; :(::)];

  / 3.) test plan execution, don't loop on failure
  loop    :max 0,qdInstructions`loop;
  function:last function;
  input   :last input;
  res     :trap[.Q.ts[function];enlist input];

  if[first res; reply[`execution;;] . res; :(::)];

  res[1;0]:$[loop;
      [start:.z.p; do[loop;function input]; end:.z.p; (end-start;res[1;0;1])];
      ("n"$1000000*res[1;0;0];res[1;0;1])
  ];
  reply[`execution;;] . res;

  / 4.) smash
  smash:trap[interpret["q"]@';qdInstructions`smash];
  if[first smash; reply[`smash;;] . smash; :(::)];
 };
\d .test

// ============
//  Evaluation
// ============
.test.evaluate:{[oPlan]
  if[.is.qt[oPlan]; :.z.s'[oPlan]];
  .test.is.error.plan[oPlan];

  RESULTS upsert oPlan,(1#`pass)!1#.test.i.evaluate[oPlan];
 };

.test.i.evaluate:{[oPlan]
  case:CASES[o.case.construct oPlan];
  nature:case`nature;
  method:case`method;

  / Return false if test case not executed
  if[not RESULTS[oPlan][`executed]; :0b];

  / Return false if test case failed but not if the failure is what's expected
  if[RESULTS[oPlan][`failed];
    if[not (`fail~method) & RESULTS[oPlan][`execution]; :0b]
  ];

  / Evaluate CASES`method`assert
  $[`value    ~ nature; .test.eval.value[oPlan];
    `property ~ nature; .test.eval.property[method;oPlan];
                        '.test.h.log.error "Unrecognised nature of test case: ",-3!nature
  ];

  / Assert the output
  method:.test.get.eval[oPlan;`method];
  $[`value    ~ nature  ; .test.assert.value[oPlan];
    `property ~ nature  ; .test.assert.property[method;oPlan];
  ]
 };

/ IDEA group all evaluation functions together as a part of later cleanup?
.test.eval.value:{[oPlan]
  .test.is.error.plan[oPlan];

  case:CASES[o.case.construct oPlan];

  .test.worker.eval[oPlan;`method] . method:.test.h.trap.apply[-6!;case`method];
  if[first method; :(::)];

  .test.worker.eval[oPlan;`assert] . assert:.test.h.trap.apply[-6!;case`assert];
  if[first assert; :(::)];
 };

.test.eval.property:{[sProperty;oPlan]
  .test.is.error.property[sProperty];
  .test.is.error.plan[oPlan];

  .test.eval[sProperty][oPlan]
 };

.test.eval.pass:{[oPlan]
  .test.is.error.plan[oPlan];

  case:CASES[o.case.construct oPlan];
  .test.worker.eval[oPlan;`method] . (0b;case`method);
  .test.worker.eval[oPlan;`assert] . (0b;::);
 };

.test.eval.fail:{[oPlan]
  .test.is.error.plan[oPlan];

  case:CASES[o.case.construct oPlan];

  / Lenient evaluation of `c`cl
  assert:.test.h.trap.apply[-6!;case`assert];
  if[not[assert 0] & .is.c[assert 1]; assert[1]:(),assert[1]];

  .test.worker.eval[oPlan;`method] . (0b;case`method);
  .test.worker.eval[oPlan;`assert] . assert;
 };

.test.eval.success:{[oPlan]
  .test.is.error.plan[oPlan];

  case:CASES[o.case.construct oPlan];
  .test.worker.eval[oPlan;`method] . (0b;case`method);
  .test.worker.eval[oPlan;`assert] . (0b;::);
 };

.test.eval.not:{[oPlan] .test.eval.is[oPlan]};

.test.eval.is:{[oPlan]
  .test.is.error.plan[oPlan];

  case:CASES[o.case.construct oPlan];
  .test.worker.eval[oPlan;`method] . (0b;case`method);

  assert:case`assert;
  assert:$[.is.ns[assert]; assert; ` sv `.is,assert];
  assert:$[.miq.fn.is.address[assert];
      (0b;assert);
      (1b;("Property assertion not available: ",-3!assert;()))
  ];

  .test.worker.eval[oPlan;`assert] . assert;
 };

.test.eval.count:{[oPlan]
  .test.is.error.plan[oPlan];

  case:CASES[o.case.construct oPlan];
  .test.worker.eval[oPlan;`method] . (0b;case`method);

  assert:.test.h.trap.apply[-6!;case`assert];
  if[first assert; .test.worker.eval[oPlan;`assert] . assert; :(::)];

  assert:last assert;
  assert:$[.is.whole[assert];
      (0b;`long$assert);
      (1b;("Property 'count' expects a whole number to assert: ",-3!assert;()))
  ];

  .test.worker.eval[oPlan;`assert] . assert;
 };

// ===========
//  Assertion
// ===========
/ Helper
.test.assert.i.verify:{[oPlan;x]
  if[.is.b[x]; :x];
  RESULTS upsert oPlan,`failed`assertion!11b;
  ERRORS  upsert oPlan,`error`stack!("Property test didn't yield boolean: ",-3!oPlan;());
  0b
 };

/ Assertions
/ TODO trapping of assertion tests
.test.assert.value:{[oPlan]
  .test.is.error.plan[oPlan];

  method:.test.get.eval[oPlan;`method];
  assert:.test.get.eval[oPlan;`assert];
  output:.test.get.eval[oPlan;`output];

  if[2<>.miq.fn.arity[method]; '.test.h.log.error "Value test not binary: ",-3!method];
  .test.assert.i.verify[oPlan] method[output;assert]
 };

.test.assert.property:{[sProperty;oPlan]
  .test.is.error.property[sProperty];
  .test.is.error.plan[oPlan];

  .test.assert.i.verify[oPlan] .test.assert[sProperty][oPlan]
 };

.test.assert.pass:{[oPlan]
  .test.is.error.plan[oPlan];
  1b
 };

.test.assert.fail:{[oPlan]
  .test.is.error.plan[oPlan];

  assert:.test.get.eval[oPlan;`assert];
  if[not .is.cl[assert]; '.test.h.log.error "`fail method requires a string to assert: ",-3!assert];

  $[11b~RESULTS[oPlan][`failed`execution];
      ERRORS[oPlan][`error] like assert;
      0b
  ]
 };

.test.assert.success:{[oPlan]
  .test.is.error.plan[oPlan];

  10b~RESULTS[oPlan][`executed`failed]
 };

.test.assert.is:{[oPlan]
  .test.is.error.plan[oPlan];

  assert:.test.get.eval[oPlan;`assert];
  output:.test.get.eval[oPlan;`output];

  assert[output]
 };

.test.assert.not:{[oPlan] not .test.assert.is[oPlan]};

.test.assert.count:{[oPlan]
  .test.is.error.plan[oPlan];

  assert:.test.get.eval[oPlan;`assert];
  output:.test.get.eval[oPlan;`output];

  assert~count output
 };

// ==========
//  Batching
// ==========
.test.batch.id:{[] ` sv (`$4?.miq.u.text.A),(`$raze string 7?10)};

/ TODO make this shite better, i.e. don't use globals but use iterator?
.test.batch.queue:{[]
  sequence:flip (CASES[] o.case.construct'[key QUEUE])[`build`smash];
  batches:{[x;y] $[first[y 1]|last[y 0]; .test.batch.id[]; x]};

  update batch:batches\[.test.batch.id[];flip(enlist[00b],1_prev@;::)@\:sequence] from QUEUE;
  BATCHES set TT.BATCHES upsert select cases:count case by name:batch from QUEUE;
 };

.test.batch.status :{[] BATCHES[] lj select todo:count[completed]-count[where completed] by name:batch from QUEUE};
.test.batch.pending:{[] exec name from BATCHES where not initiated};
.test.batch.ongoing:{[] exec name from BATCHES where ongoing};
.test.batch.done   :{[] exec name from BATCHES where completed|terminated};

.test.batch.init:{[sBatch]
  .test.is.error.batch[sBatch];

  workerid:first .test.h.worker.new[1;`port;::];
  update worker:workerid, initiated:1b, ongoing:1b, start:.z.p from BATCHES where name=sBatch;
 };

.test.batch.worker:{[sBatch]
  .test.is.error.batch[sBatch];
  BATCHES[sBatch][`worker]
 };

.test.batch.next:{[sBatch]
  .test.is.error.batch[sBatch];

  / IDEA make this fail if there is no queued plan to return?
  first select from (0!QUEUE[]) where batch=sBatch, not executed
 };

.test.batch.complete:{[sBatch]
  .test.is.error.batch[sBatch];

  .test.h.log.info "Batch completeded: ",-3!sBatch;
  update ongoing:0b, completed:1b, finish:.z.p from BATCHES where name=sBatch;
  .test.h.worker.disconnect[BATCHES[sBatch][`worker]]
 };

.test.batch.terminate:{[sBatch]
  .test.is.error.batch[sBatch];

  .test.h.log.info "Batch terminated: ",-3!sBatch;
  update terminated:1b from QUEUE where batch=sBatch, executed & not completed;
  update ongoing:0b, terminated:1b from BATCHES where name=sBatch;
  .test.h.worker.kill[BATCHES[sBatch][`worker]]
 };

// ========
//  Remote
// ========
.test.remote.start:{[jInterval]
  .is.error.any[`none`j;jInterval];
  system"t ",string $[.is.none[jInterval]; C.REMOTE.TIMER[]; jInterval]
 };

.test.remote.stop:{[] system"t 0"};

.test.remote.terminate:{[]
  .test.remote.stop[];
  .test.batch.terminate'[.test.batch.ongoing[]];
  .test.remote.clear[]
 };

/ IDEA make this targeted?
.test.remote.clear:{[]
  QUEUE   set 0#QUEUE[];
  BATCHES set 0#BATCHES[];
 };

/ NOTE Will work on 'untouchable queue' principle in the interim for simplicity
.test.remote.queue:{[olPlan]
  .miq.object.is.error.class[O.PLAN]'[olPlan];
  QUEUE upsert olPlan;
  .test.batch.queue[];
 };

.test.remote.ongoing:{[] select from QUEUE where executed, not (terminated|completed)};

.test.remote.execute:{[jWorker;oPlan]
  .test.is.error.plan[oPlan];

  instructions:PLANS[oPlan];
  message:(.test.i.execute;.miq.qk.interpret;get .test.h.trap.apply;oPlan;instructions);

  .test.h.log.info "Dispatching test case to worker: ",-3!oPlan;
  .test.worker.send[jWorker;message];
 };

.test.remote.serial:{[]
  .test.remote.i.serial'[.test.batch.pending[]];
  .test.complete[];
 };

.test.remote.i.serial:{[batch]
  .test.batch.init[batch];

  fail   :0b;
  worker :.test.h.worker.info[BATCHES[batch][`worker]];
  address:worker[`address];
  handle :worker[`handle];

  while[not[fail] & 0<first exec todo from .test.batch.status[] where name=batch;
    job   :.test.batch.next[batch];
    worker:.test.batch.worker[job`batch];
    plan  :o.plan.construct[job];

    .test.remote.execute[worker;plan];
    QUEUE upsert plan,`executed`start!(1b;.z.p);
    .test.h.ipc.async[handle;({neg[.z.w]`done};::)];

    h:@[hopen;(address;C.REMOTE.TIMEOUT[]);0Ni];
    $[null h;
        fail:1b;
      [
        hclose h;
        while[not`done~reply:handle[]; 0 reply];
        .test.evaluate[plan];
        QUEUE upsert plan,(1#`evaluated)!1#1b
      ]
    ];
  ];

  $[fail; .test.batch.terminate[batch]; .test.batch.complete[batch]]
 };

.test.remote.timer:{[x]
  / Enforce timeout and discard whole batch on ocurrence
  timeout:x-C.REMOTE.TIMEOUT[]*1000000;
  terminate:exec distinct batch from .test.remote.ongoing[] where start<timeout;
  .test.batch.terminate'[terminate];

  / TODO .test.batch.status[] could very well be remade into .test.batch.complete[] component
  / Disconnect from workers of completed batches
  completed:exec name from .test.batch.status[] where not completed, 0=todo;
  .test.batch.complete'[completed];

  / Spawn new workers up to the configured limit & update queue
  if[spawn:C.REMOTE.WORKERS[] - count .test.batch.ongoing[];
    .test.batch.init'[spawn sublist .test.batch.pending[]]
  ];

  / Fetch test cases next in line of execution
  todo:.test.batch.ongoing[] except exec batch from .test.remote.ongoing[];
  todo:.test.batch.next'[todo];

  / Execute on remote and mark as executed in queue
  {[job]
    worker:.test.batch.worker[job`batch];
    plan  :o.plan.construct[job];
    .test.remote.execute[worker;plan];
    QUEUE upsert plan,`executed`start!(1b;.z.p);
  } each todo;

  / Assert completed work
  evaluate:key select from QUEUE where completed, not evaluated;
  .test.evaluate[evaluate];
  QUEUE upsert update evaluated:1b from evaluate;

  / Stop timer when queue processing done
  if[not count select from QUEUE where not (terminated|completed);
    .test.remote.stop[];
    .test.complete[];
  ];
 };

/ NOTE TEMPORARY HACK
.z.ts:.test.remote.timer;

// =========
//  Workers
// =========
.test.worker.send:{[jWorker;message]
  handle:.test.h.worker.info[jWorker][`handle];
  .test.h.ipc.async[handle;message]
 };

.test.worker.reply:{[oPlan;sStage;bFail;output]
  .test.is.error.plan[oPlan];
  .is.error[`s`b]@'(sStage;bFail);

  .test.h.log.debug "Reply for test plan: ",-3!oPlan;
  $[sStage in `function`input`method`assert`output;
      .test.worker.eval[oPlan;sStage;bFail;output];
      .test.worker.result[oPlan;sStage;bFail;output]
  ];
 };

/ Capture output of remote evaluation
.test.worker.eval:{[oPlan;sStage;bFail;out]
  if[bFail;
    RESULTS upsert oPlan,(1#`evaluation)!1#1b;
    EVALS   upsert oPlan,(1#`stage)!enlist $[C.EVAL.SERIALISE[];-8!sStage;sStage];
    ERRORS  upsert oPlan,`error`stack!out;
  ];

  if[not bFail;
    EVALS   upsert oPlan,(1#sStage)!enlist $[C.EVAL.SERIALISE[];-8!out;out];
  ];
 };

/ TODO good candidate for templating? (`cl;`generic) is failure, (`n;`) is success, everything else is bullshit
/ TODO we can use jWorker as identifier instead of test as it can be linked via batch? but why would I do that...
.test.worker.result:{[oPlan;sStage;bFail;out]
  .test.h.log.info "Results for test plan: ",-3!oPlan;
  / IDEA push timer forward as we have likely made a available
  .test.worker[$[bFail;`failure;`success]][oPlan;sStage;;] . out;

  / Dirty
  if[oPlan in key QUEUE; QUEUE upsert oPlan,`completed`finish!(1b;.z.p)];
 };

.test.worker.success:{[oPlan;sStage;ts;out]
  RESULTS upsert oPlan,`executed`time`space!(1b;ts 0;ts 1);
  EVALS   upsert oPlan,(1#`output)!enlist $[C.EVAL.SERIALISE[];-8!out;out];
 };

.test.worker.failure:{[oPlan;sStage;clError;stack]
  result :oPlan,((`failed,sStage)!11b);
  result,:$[`execution~sStage;(1#`executed)!1#1b;()];

  RESULTS upsert result;
  EVALS   upsert oPlan,(1#`output)!enlist $[C.EVAL.SERIALISE[];-8!();()];
  ERRORS  upsert oPlan,`error`stack!(clError;stack);
 };


// ========
//  Script
// ========
\d .
if[.miq.main[];
  / Import logging
  .miq.handler.import[`stl.log;`];

  / Add STL path to library if found, leave dependency management to user if not
  if[.miq.fs.is.dir stl:.miq.fs.path.absolute[first ` vs -1!.z.f;`:miQ/STL];
    / TODO need functions to add / remove code source paths
    .log.info "Adding directory to module path: ",-3!stl;
    .miq.config.set[`module.module.directory; .miq.config.get[`module.module.directory],stl];
    .miq.config.apply[];
  ];

  / Load arg module and handle script arguments
  .miq.handler.get[`miq.module.load][`module;`arg];

  .arg.config.set[`opts] flip`opt`cast`alias`description!flip (
    (`root              ;`fs    ;0#`  ;"Directory to \\cd to"                                                                           )
   ;(`source            ;`fs    ;0#`  ;"Location of test definitions, accepts both file and a directory"                                )
   ;(`extension         ;`sl    ;0#`  ;"List of test file extensions to match when exploring directory tree"                            )
   ;(`workers           ;`j     ;0#`  ;"Number of workers used to parallelise the testing"                                              )
   ;(`timeout           ;`j     ;0#`  ;"Terminate test cases breaching this threshold in milliseconds"                                  )
   ;(`timer             ;`j     ;0#`  ;"Asynchronous loop frequency in milliseconds"                                                    )
   ;(`serial            ;`b     ;0#`  ;"Run test cases serially using a single worker"                                                  )
   ;(`output            ;`fs    ;0#`  ;"Store all of the generated results and internal state into a file"                              )
   ;(`output.summary    ;`fs    ;0#`  ;"Same as [output], but stores only the final summary"                                            )
   ;(`summary           ;`b     ;0#`  ;"Print only summary, will not execute any test cases"                                            )
   ;(`coverage          ;`b     ;0#`  ;"Print out only aggregated test coverage, will not execute any test cases"                       )
   ;(`noexit            ;`b     ;0#`  ;"Will not exit automatically when no connection handles are open and not connected to terminal"  )
   ;(`port              ;`port  ;`p   ;"Port to listen on"                                                                              )
   ;(`port.range        ;`jr    ;0#`  ;"Restrict listener to this range"                                                                )
    );

  .arg.config.set[`program.description;"miQ.test - a parallelised asynchronous testing, performance and coverage engine"];
  .arg.config.set[`program.usage;"miq test.q -source <DIR|FILE> [options] ..."];
  .arg.config.set'[`assert.auto`assert.exit`help.auto;1b];
  .arg.config.set[`mandatory;1#`source];
  .arg.init[];

  / Load & configure miQ.test dependencies
  .miq.handler.get[`miq.module.load][`module]'[`ipc`worker];
  .ipc.config.set[`flush;1b];
  .ipc.init[];
  .worker.init[];

  / Non-interactive execution has to supply either -port or -serial
  if[not[.miq.system.is.terminal[]] & not any .arg.is.supplied'[`port`serial];
    .log.error "Neither [port] not [serial] options have been supplied!";
    .log.error "These are mandatory for non-interactive execution!";
    .arg.usage[];
    exit 1
   ];

  / Output file configuration
  if[any .arg.is.supplied'[`output`output.summary];
    if[all .arg.is.supplied'[`output`output.summary];
      '.log.error "[output] and [output.summary] are mutually exclusive!";
      .arg.usage[];
      exit 1
    ];
    .test.config.set[`output.persist;1b];
    .test.config.set[`output.file] .miq.fs.to.absolute $[.arg.is.supplied[`output];
        .arg.get`output;
        [.test.config.set[`output.summary;1b]; .arg.get`output.summary]
    ];
  ];

  if[.arg.is.supplied[`root]; system"cd ",1_string .arg.get[`root]];

  if[.arg.is.supplied[`port];
    system"p ",.ipc.to.listener $[
      0i~last .arg.get[`port];
        first .ipc.port.random[1;] $[.arg.is.supplied[`port.range]; .arg.get[`port.range]; .ipc.port.unprivileged[]];
        .arg.get[`port]
    ]
  ];

  if[.arg.is.supplied[`noexit]; .test.config.set[`complete.exit;0b]];

  / Configure & Initialise the test module
  / Map test's configuration items to script arguments and action
  {[x]
    opts:where .arg.is.supplied'[x];
    .test.config.set'[opts;.arg.get'[x opts]]
  } `file.extension`remote.workers`remote.timeout`remote.timer`execute.serial!`extension`workers`timeout`timer`serial;

  .test.init[];
  .test.load[.arg.get[`source]];

  / Run the tests, unless only coverage / summary was requested
  if[not any .arg.is.supplied'[`summary`coverage]; .test.run[]];
  if[.arg.is.supplied[`summary] ; .test.h.log.info "\n",.Q.s .test.summary[]];
  if[.arg.is.supplied[`coverage]; .test.h.log.info "Code coverage: ",-3!exec 100*count[where tested]%count[tested] from .test.coverage[]];
 ];

/
/ SCRIPT
TODO pass debug flags
TODO configurable threshold for passed test cases?

/ MODULE
TODO figure out how to make remote evaluation and assertion possible and configurable
TODO .test.debug[test] to pause testing right before execution so we can play around with test case?
TODO .test.step.*[] - allow going though the test cases step-by-step, control by next, next 5, continue, end, ...
     this will allow user to debug behavioural tests
TODO test file syntax checking
     - incorrect number of fill arguments
     - incorrect number of case fields
     - configurable warn on some other gotchas?
     - will have to forgo input / output resolution
     - enable internal SYNTAX flag to prevent storing of tests / code evaluation
TODO remember origin of AUGMENT: code so we can clear[] it?
TODO check for changes to myself? so when AUGMENT: is called, we can clear it later on
TODO when parsing code, make sure the definitions contain file name at least!
     somehow make line numbers appear as well? but how? (every section to remember line number?)
     can it be done? INCLUDE is messing up my line numbers...
TODO syntax highlight for *.miqt files
IDEA CLEANUP: section to clean up any mess?
IDEA or maybe smash.always to run smash even if test case fails?
TODO new field to say "yep, this case is OK to fail"? i.e. don't cause non-zero exit on failure of this task?

