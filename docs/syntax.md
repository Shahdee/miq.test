Syntax
===
---
This page documents the test file syntax that is used to define the test cases and their context.
* Every file is formed of sections and defines a single test suite
* Sections define logical and runtime context of test cases
* Sections can be defined as being either
  * **TOP level** - defined before any `STEP` section declaration
  * **STEP level** - defined after `STEP` section has been declared
* Comments use the standard k/q code comment character `/`
* Sections that can be defined using code accept both `k` and `q`

Short summary of sections and their hierarchy:

| Section name  | Level     | Mandatory | Description                                                               |
| ------------- | --------- | --------- | ------------------------------------------------------------------------- |
| `NAME`        | top       | No        | Top level test name that all following definitions are nested under       |
| `COVERAGE`    | top       | No        | List of functions unit testing needs to cover                             |
| `AUGMENT`     | top       | No        | Additional code executed by miQ.test during test parsing                  |
| `INCLUDE`     | top, step | No        | Include content of another file as if it was where the `INCLUDE` is       |
| `BUILD`       | top, step | No        | Code to run before test case execution, top level applies to all steps    |
| `SMASH`       | top, step | No        | Code to run after test case execution, top level applies to all steps     |
| `STEP`        | top       | Yes       | Create a new step to nest `STEP` level sections under                     |
| `CASE`        | step      | Yes       | Actual test definitions                                                   |

# Description

* To describe a `COVER`, `BUILD`, `SMASH`, or `STEP` section: Any number of continuous non-empty lines following section declaration that begins with `"` will be picked up and used as section description.
* To describe a test case under `CASE` section: Lines starting with `"` that appear immediatelly before test case definition will be treated as its description. Ignores empty lines.

> [!TIP] All descriptions can be multiline.

# File inclusion
Some sections allow inclusion of other files instead of defining the section within the test file itself. These files will be looked up under subdirectory named after given section. This subdirectory has to be located inside the same directory that the current test script is located.

* `include` - accepts a list of files over multiple lines
* `augment` - accepts a list of files, if provided on a section declaration line
* `build` - a single section can include only a single file only
* `smash` - a single section can include only a single file only

> [!NOTE] This also means that subdirectories bearing lower-case names of these sections won't be included in the `.test.load[]` expansion when targetting directory instead of file

# Scenarios
Scenario is a combination of multiple `build` instructions into a single one. This is achieved by crossing global-level build definitions with `step`-level build definitions. To distinguish between individual build and smash instructions, they have to be *named*. If the name is not unique on its level, only the last definition will be sourced.

In the following example, the miQ.test will:
* Define global level build / smash instructions
* Define step level build / smash instructions
* Cross build instructions from both levels to create scenarios `` `case.A`step.1 `` and `` `case.B`step.1 ``
* Smash instructions of matching name on particular level will be executed as well if defined
* Execute test case independently under both scenarios
* Combine the results and report as passing only if both scenarios pass

Example:
```
NAME: scenario

/ build and smash instructions not under STEP are considered global
BUILD: case.A
a:12

BUILD: case.B
a:22

/ case.B will leave b variable defined after finishing as there's no case.B smash definition
SMASH: case.A
delete a from `.

SMASH: case.b           / typo means this is an orphan and will never execute
delete a from `.


STEP:
/ Now define STEP-level build / smash instructions

BUILD: step.1
b:neg a

SMASH: step.1
delete b from `.

CASE:
" Single test case, 0 loops, build, smash
# 0 1 1

" a should be negative; x - output of get@`..a; y - assert value
get ; `..a  ; {x<y} ; 0
```

# Sections
All section definitions are declared using their capitalised names followed by a colon and their specific syntax.

## NAME
By default, the name that tests belong to is derived from the file name defining them, but this can be overriden using `NAME` section. Name is always a symbol and it is obtained in the following manner:
1. Uncomment all lines up to next section
1. Raze all uncommented lines
1. Cast the resulting string to symbol
1. Change the context of parsing to fall under the new test name

Example:
```
NAME: Michal
```

> [!ATTENTION] Definition of multiple test names in a single file is not supported yet.

## AUGMENT
Agumentation is a process of loading additional code into local miQ.test process to facilitate further functionality. It is expected to be followed by a valid k/q code and will execute immediatelly.

Example:
```
/ Direct definition
AUGMENT:
\l STL/env.q
.env.init[]

/ Definition through inclusion
AUGMENT: env.miqt
```

## COVERAGE
Define functions that unit testing (for current `NAME`) is expected to cover. The definition is a valid k/q code whose last statement returns a list of addresses (type `ns`).
* How to [describe](#description)

> [!ATTENTION] As of now, the coverage definition will be evaluated on a local process. You may need to `AUGMENT` the process in order for coverage definition to return the expected list of addresses.

Example:
```
COVERAGE:   / Code
" Public API of env module
raze @[;`op`fn`fp`qc`qi] group {x!(')[.is.type;get]'[x]} exec name from
  group .miq.ns.prune[`M`CFG`config`z`i`t`h`warn`error] .miq.ns.trim[``init] .miq.ns.tree[`.env]

COVERAGE:   / Listing
`.env.vars`.env.build`.env.get`.env.set`.env.send`.env.is.variable`.env.is.interned
```

## INCLUDE
Include another file into current test file. It will behave exactly as if the content of the included file was an explicit part of current test file. List of files to include is obtained in the following manner:
1. Uncomment all lines up to next section
2. Parse every line independently looking for file symbols or plain text
3. Take file symbols and cast plain text to file symbols
4. Raze the results to obtain a single list
5. Translate every file location into address relative to current working directory
6. Read each file and have it parsed as miQ.test file

> [!NOTE] Recursion is disabled for now.

Example:
```
INCLUDE:
`:file1.miqt    / build instructions
  file2.miqt    / smash instructions
```

## BUILD
Defines the context for test case execution. Expects valid k/q code, which will be executed before every test case with `build` flag enabled.
* How to [describe](#description)
* How to [combine](#scenarios) build instructions into scenarios

Example:
```
/ plain, will bear a name of `default
BUILD:
.my.function:{x#y}

/ named build instructions
BUILD: function
.my.function:{x#y}

/ named file inclusion
BUILD: function my.fn.miqt


/ File content has to be a plain k/q code
> cat my.fn.miqt
.my.function:{x#y}
```

## SMASH
Define the steps necessary to return the state of the process to what it was before the build has been executed. Expects valid k/q code, which will be executed after every test case with `smash` flag enabled.
* How to [describe](#description)
* How to [combine](#scenarios) smash instructions into scenarios
* Naming and inclusion follows the exact same rules as [build](#build)

> [!TIP] Smashing may not be necessary at all if all of the execution takes place on throw-away remote workers.

## STEP
Define a new step logical context, allowing to nest `build`, `smash` and `case` sections within. A test can have any number of steps.

## CASE
The most important part of the test definition. Here one defines what to test and how to test it. Test cases can be spread over multiple lines, and each part of it can be either in `q` or `k`. The syntax is as follows:

```
" Description
function ; input ; method ; assert ; loop ; build ; smash
```

| field         | Stored as     | Description                                                                                                   |
| ------------- | ------------- | ------------------------------------------------------------------------------------------------------------- |
| `function`    | K-tree        | Code accepting `input`                                                                                        |
| `input`       | K-tree        | Passed over to `function` using `@` apply                                                                     |
| `method`      | K-tree        | Method of output assertion                                                                                    |
| `assert`      | K-tree        | Expected value for value-type tests or a property descriptor for property-type tests                          |
| `loop`        | long          | Non-negative integer defining the number of `function` executions for performance testing (time and space)    |
| `build`       | boolean       | Determines if the build instructions are executed prior to test case execution                                |
| `smash`       | boolean       | Determines if the smash instructions are executed after the test case execution                               |

Example:
```
k).my.function@@0x00;           / Defines a projection of .my.function
k)`sym                          / semicolon is not mandatory
~;`sym`sym`sym`sym;1000;0;0     / finish the test case definition

/ Hypothetical definition ---> .my.function:{x#y}
" Return 2#`sym
.my.function .  ; (2;`sym)  ; ~     ; 2#`sym    ; 1000  ; 1 ; 1     / Passes
" Take fails here
.my.function .  ; (`s;`sym) ; fail  ; "type"    ; 0     ; 0 ; 0     / Passes because the signal will match the expression: 'signal' like "type"
" If not unary, functions have to be dot-projected
.my.function    ; (2;`sym)  ; ~     ; `sym`sym  ; 0     ; 0 ; 0     / Will not pass, because execution will return projection: .my.function[(2;`sym);]
```

### Method & Assert
Every assertion has to return a boolean atom. There are 2 types of assertion methods:
* `value` - comparing `function@input` output with `assert`, so in effect it is a binary function fed both as an input
* `property` - making sure the `function@input` output has a desired property as defined by combination of `method` and `assert` fields

#### Value
Any assertion method that is not classified as type `property` is considered to be of type `value`. Assertion method is derived from test case using the following formula `method[function@input;assert]`, where `method`:
* Is any (named) lambda
* Accepts 2 input parameters
* Returns a boolean atom

Example of `value` method:
```
/ {x=y+1} -> x is output of {x+1}@1; y is assert value (1)
/ {x=y+1}[{x+1}@1;1] is the operation that will return the result of assertion
{x+1} ; 1 ; {x=y+1} ; 1 ; 0 ; 0 ; 0
```

#### Property

### Loop



### # (Fill)
Where we have repeating loop, build \& smash values, we can use fill instruction to pre-fill the values:
```
/ loop 1000 times, don't build and don't smash
fn  ; 1     ; ~     ; 10 ; 1000 ; 0 ; 0
fn  ; 2     ; ~     ; 20 ; 1000 ; 0 ; 0
fn  ; 3     ; ~     ; 30 ; 1000 ; 0 ; 0

/ Same as above but loop / build / smash declared at fill level
# 1000 0 0
fn  ; 1     ; ~     ; 10
fn  ; 2     ; ~     ; 20
fn  ; 3     ; ~     ; 30
```

