Quick start
===
---

# Run as script
```
miq test.q -source <DIR|FILE> [options] ...
```
* `miq` is simply a miQ enabled q. Check out [how to embed miQ here](http://51.15.72.72:3000/#/?id=embedding).
* for options, run `miq test.q -help`

# Load as module
* Load `test.q` into miQ enabled q
* Configure using `.test.configure.set[]` to update configuration pre-init
* Initiate using `.test.init[]`
* Load test definition using `.test.load[]`
* Run loaded tests using `.test.run[]`

# Inspect work done
* loaded test definitions: `.test.steps[]`, `.test.cases[]`, `.test.build[]`, `.test.smash[]`
* loaded coverage definitions: `.test.coverages[]`
* executed test plans and their results: `.test.plans[]`, `.test.results[]`, `.test.evals[]`, `.test.errors[]`
* consolidated information on executed tests: `.test.report[]`, `.test.coverage[]`, `.test.summary[]`
* internal execution state: `.test.queue[]`, `.test.batches[]`

# Modus operandi
* Load test definitions
* Derive test plans from test cases by crossing them with cross of global \& step level build instructions
* Group test plans that execute consecutively into batches
* Construct test plan queue for batches
* Execute test plan queue batch-wise in parallel
  * Execute build instructions if requested
  * Evaluate K-tree of *function* and *input* part of test case definition
  * Execute `function@input`
  * If above didn't fail, attempt performance testing by executing above `loop` times and measure the time it took to complete
  * Execute smash instructions if requested
* Store all evaluation details, and so do all execution results and errors
* Assert the results
  * Evaluate K-tree of *method* and *assert* part of test case definition
  * If *method* is a q code, perform value assertion `code[output;assert]`
  * If *method* refers to property, derive the property assertion from *method* and *assert* and perform `property[output]`
* Mark test plan as `pass` if it succeeds
* Run `.test.complete[]` to show summary and coverage information

> [!NOTE] When execting test plans serially, they are not batched but simply executed \& asserted immediately in the order they have been defined.

# Example test file
* Check out the [full syntax here](syntax)
* Examples of individual test files:
  * [arg.miqt](https://gitlab.com/Shahdee/miq/-/blob/master/test/unit/arg.miqt)
  * [ipc.miqt](https://gitlab.com/Shahdee/miq/-/blob/master/test/unit/ipc.miqt)
* Minimalistic example of test file `example.miqt` testing imaginary `example.q` module below

```
COVERAGE:
`.example.fn1`.example.fn2

BUILD:
" Load the code
\l example.q

STEP:
" example tests

CASE:

/ Simple comment
" Adds 1 to the input
.example.fn1    ; 1     ; ~     ; 2 ; 1000 ; 1 ; 1
" Counts number of inputs
.example.fn2    ; ````` ; ~     ; 5 ; 1000 ; 1 ; 1
```




