* Documentation: [https://shahdee.gitlab.io/miq.test](https://shahdee.gitlab.io/miq.test/#/)
* Author       : Michal Širochman / [LinkedIn](https://www.linkedin.com/in/sirochman)

miQ.test
===
Parallelised asynchronous testing framework built on top of [miQ kernel](https://shahdee.gitlab.io/miq/#/) and [STL modules](http://51.15.72.72:3000/#/modules). Use it as module or execute as script.

* Definition of tests that are as simple or as complex as you want them to be
* Capacity to perform unit, integration, and system tests
* Performance testing - capture time and space taken for execution
* Stores test case definitions in k-tree for on-demand evaluation
* Capturing signal & related execution stack of test cases that fail
* Detailed overview of what stage of test case execution failed

# Execution
Delivered as an executable module. Try it out yourself using miQ kernel's unit tests:
```
QINIT=miQ/miq/miq.q q test.q -root miQ/ -source test/unit/miq -workers 10 -timeout 60000 -coverage
...
q).test.summary[]
name    | cases passed scenarios success failure coverage
--------| -----------------------------------------------
fd      | 7     7      14        14      0       85.71429
fq      | 2     2      4         4       0       100
miq     | 7     7      14        14      0       100
ns      | 36    36     72        72      0       94.44444
system  | 9     9      18        18      0       80
template| 35    35     134       134     0       71.42857
u       | 48    48     96        96      0       100
bb      |                                        0
cfg     |                                        0
colour  |                                        0
fn      |                                        0
fs      |                                        0
...
```

For help, simply ask:
```
MIQ_SILENT_BOOT=true QINIT=miQ/miq/miq.q q test.q -help
```

## Project status
### Functional
| Objective                 | Status    | Documented    | Note                                                              |
| ------------------------- | --------- | ------------- | ----------------------------------------------------------------- |
| Test syntax               | Good      | No            | Core syntax will likely remain unchanged                          |
| Parsing into k-tree       | Good      | No            | Instructions are either evaluated immediately or stored in k-tree |
| k-tree evaluation         | Good      | No            | On-demand evaluation of stored instructions                       |
| Value assertion           | Good      | No            | Assert value of evaluation result                                 |
| Property assertion        | Good      | No            | Assert property of evaluation result                              |
| Serial execution          | Good      | No            | Execute test cases sequentially on remote process                 |
| Parallelised execution    | Good      | No            | Execute test cases in parallel on remote process                  |
| Local and remote mode     | Good      | No            | Execution of test cases possible both on local and remote process |
| Performance testing       | Good \*   | No            | Measures time and space, but cannot compare to previous runs yet  |
| Debugging                 | NYI       | No            | Execution will pause and allow manual inspection                  |
| Test plan walk            | NYI       | No            | Walk through test case execution to improving debugging           |
| Test coverage             | NYI       | No            | Report to output and / or a file the test coverage                |
| Test results dump         | NYI       | No            | Persist test results for later inspection                         |
| Scenarios                 | NYI       | No            | Allow execution of test cases under multiple scenarios            |

### Technical
| Objective                 | Status    | Note                                                      |
| ------------------------- | --------- | --------------------------------------------------------- |
| Loadable module           | Done      |                                                           |
| Scripted execution        | Done      |                                                           |
| Interface export          | Done      | Temporary solution, dumping of all non-internal routines  |
| timer module integration  | Missing   | currently `.z.ts` is hardcoded                            |

